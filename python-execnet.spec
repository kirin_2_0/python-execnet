Name:           python-execnet
Version:        1.9.0
Release:        2
Summary:        Rapid multi-Python deployment
License:        MIT and GPLv2+
URL:            http://codespeak.net/execnet
Source0:        https://pypi.io/packages/source/e/execnet/execnet-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  procps-ng
%description
execnet provides carefully tested means to ad-hoc interact with Python interpreters across version,
platform and network barriers. It provides a minimal and fast API targetting the following uses:
distribute tasks to local or remote processes
write and deploy hybrid multi-process applications
write scripts to administer multiple hosts

%package   -n   python3-execnet
Summary:        Rapid multi-Python deployment
BuildRequires:  python3-apipkg python3-devel python3-pytest python3-setuptools
BuildRequires:  python3-setuptools_scm python3-sphinx python3-py
%{?python_provide:%python_provide python3-execnet}

%description -n python3-execnet
execnet provides carefully tested means to ad-hoc interact with Python interpreters across version,
platform and network barriers. It provides a minimal and fast API targetting the following uses:
distribute tasks to local or remote processes
write and deploy hybrid multi-process applications
write scripts to administer multiple hosts

%prep
%autosetup -n execnet-%{version} -p1

find . -type f -a \( -name '*.py' -o -name 'py.*' \) \
   -exec sed -i '1{/^#!/d}' {} \; \
   -exec chmod 644 {} \;

%build
SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%py3_build
make -C doc html PYTHONPATH=$(pwd)
rm -f doc/_build/html/.buildinfo

%install
SETUPTOOLS_SCM_PRETEND_VERSION=%{version}
%py3_install

%check
PYTHONPATH=$(pwd) LC_ALL="en_US.UTF-8" py.test-%{python3_version} -r s -k 'not test_stdouterrin_setnull' testing

%files -n python3-execnet
%license LICENSE
%doc README.rst CHANGELOG.rst
%doc doc/_build/html
%{python3_sitelib}/execnet*

%changelog
* Wed Jul 05 2023 xu_ping <707078654@qq.com> - 1.9.0-2
- Add Buildrequires python3-py to fix module 'py' has no attribute 'test'

* Fri May 27 2022 OpenStack_SIG <openstack@openeuler.org> - 1.9.0-1
- Upgrade package python3-execnet to version 1.9.0

* Mon Aug 10 2020 lingsheng <lingsheng@huawei.com> - 1.7.1-1
- Update to 1.7.1

* Thu May 21 2020 yanan li <liyanan032@huawei.com> - 1.5.0-6
- Fix asserterror for testcase.

* Wed Mar 4 2020 zhouyihang<zhouyihang1@huawei.com> - 1.5.0-5
- Pakcage init
